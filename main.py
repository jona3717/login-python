#!/usr/bin/env python

__author__ = "Jona3717"

# Este módulo inicia un menú en el que permite acceder al registro de usuarios y al inicio de sesión.

import os
import sys
from registro import Registro
from login import Login

class main(Registro, Login):
    # Heredamos de las clases Registro y Login para utilizar los métodos necesarios
    def __init__(self):
        # Constructor que ejecutará el método menu al iniciar.
        super().__init__()
        
        try:
            if sys.argv[2]:
                self.datosSesion()
            else:
                self.menu()
        except IndexError:
            self.menu()


    def menu(self):
        # Este método permite seleccionar entre registro o el inicio de sesión por medio de un menú.
        print('\n1. Registro.')
        print('2. Iniciar Sesion.\n')
        print('0. Salir.\n')

        try:
            opcion = int(input('Selecciona una opción: '))
        except ValueError:
            print('Por favor ingresa un número:')
            self.menu()

        if opcion == 1:
            os.system('clear')
            self.nuevoRegistro()
            self.reiniciar()
            self.menu()
        elif opcion == 2:
            os.system('clear')
            if self.datosSesion() is False:
                self.menu()
        elif opcion == 0:
            os.system('clear')
            exit()
        else:
            print('Opción inválida.')
            self.menu()


if __name__ == "__main__":
    main()

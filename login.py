#!/usr/bin/env python

__author__ = "Jona3717"


import hashlib
import sys
import getpass
import os
import hashlib


class Login():
    # Esta clase contendrá los métodos para el inicio de sesión

    def __init__(self):
        # Inicializa algunas variables y listas necesarias para el login
        self.num_usuario = int
        self.usuario = ""
        self.contrasena = ""
        self.usuarios = self.cargaUsuarios()
        self.contrasenas = self.cargaContrasenas()


    def accesoRapido(self):
        self.validarUsuario(True)
        self.validarContrasena()


    def cargaUsuarios(self):
        # Carga los usuarios en la lista usuarios, desde el archivo usuarios
        archivo = open('usuarios', 'r')
        for nombre in archivo:
            usuarios = nombre.split()
        return usuarios



    def cargaContrasenas(self):
        # Carga las contraseñas en la lista contrasenas, desde el archivo .contrasenas
        archivo = open('contrasenas', 'r')
        for clave in archivo:
            contrasenas = clave.split()
        return contrasenas


    def validarUsuario(self, *clave):
        # Valida que el nombre de usuario ingresado tenga un mínimo de 6 caracteres
        # y que exista en la lista
        msj = ""
        indice = 0
        if len(self.usuario) >= 4:
            for i in self.usuarios:
                if i == self.usuario:
                    self.num_usuario = indice
                    if clave:
                        self.validarContrasena()
                        msj = ""
                        break
                    else:
                        self.contrasena = getpass.getpass("Ingresa la contrasena para %s\n" % self.usuario)
                        self.validarContrasena()
                        msj = ""
                        break
                else:
                    indice += 1
                    msj ="El usuario no es válido.\n"
            if  len(msj) > 0:
                print(msj)
                self.datosSesion()
        else:
            print("El usuario no es válido\n")
            self.datosSesion()

    
    def validarContrasena(self):
        # Concede o deniega el acceso si la contraseña corresponde al usuario ingresado
        self.contrasena = hashlib.sha256(self.contrasena.encode()).hexdigest()
        if self.contrasena == self.contrasenas[self.num_usuario]:
            os.system("clear")
            print("Inicio de sesión exitoso.")
        else:
            print("Contraseña incorrecta.\n")
            self.contrasena = ""
            self.validarUsuario()

    
    def datosSesion(self):
        # Verifica si se le han pasado argumentos para el inicio de sesión rápido 
        # de no ser así, solicita los datos de inicio de sesión llamando a validarUsuario()
        try:
            if sys.argv[1].lower() == "-u":
                if sys.argv[2]:
                    self.usuario = sys.argv[2].lower()
                    self.contrasena = getpass.getpass("Ingresa la contraseña para %s\n" % self.usuario)
                    if self.contrasena == "/quit":
                        pass
                    else:
                        self.accesoRapido()
                
                elif sys.argv[2] and sys.argv[4]:
                    self.usuario = sys.argv[2]
                    self.contrasena = sys.argv[4]
                    self.accesoRapido()
                else:
                    print("No se indicó el nombre de usuario.")
            else:
                print("Argumento no válido.")
        except IndexError:
            self.usuario = input("Ingresa tu usuario:\n").lower()
            if self.usuario == "/quit":
                return False
            else:
                self.validarUsuario()


if __name__ == "__main__":
    os.system('clear')
    Login()

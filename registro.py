#!/usr/bin/env python

__author__ = "Jona3717"

# Creamos una clase que realizará el registro de usuario y una contraseña

import getpass
import hashlib
from login import Login

class Registro(Login):
    # En esta clase se encuentan los métodos para realizar el registro

    def __init__(self):
        super().__init__()
        self.usuarios = self.cargaUsuarios()
        self.contrasenas = self.cargaContrasenas()
        self.usuario = str
        self.contrasena = str
    
    
    def nuevoRegistro(self):
        self.usuario = input("Ingresa el nombre de usuario\nDebe contener como mínimo 4 caracteres: ")
        
        if self.aprobarUsuario():
            if self.usuario == "/quit":
                pass
            else:
                self.contrasena = getpass.getpass("Ingresa una contraseña con 6 caracteres como mínimo: ")
                if self.aprobarContrasena():
                    self.usuarios.append(self.usuario)
                    self.contrasenas.append(self.encriptar())
                    self.actualizarBd("usuarios")
                    self.actualizarBd("contrasenas")
                    self.reiniciar()
                else:
                    self.reiniciar()
                    self.nuevoRegistro()
        else:
            self.reiniciar()
            self.nuevoRegistro()

    
    def aprobarUsuario(self):
        usuario = self.usuario
        msj = ""
        if len(usuario) < 4:
            msj += "Ingresa un usuario válido."
            self.usuario = ""
        for i in self.usuarios:
            if i == usuario:
                msj += "\nEl usuario ya existe."
                break
        if len(msj) > 0 :
            print(msj+"\n")
            return False
        else:
            return True

        
    def aprobarContrasena(self):
        contrasena = self.contrasena
        space = False
        num = False
        msj = ""
        msj2 = ""
        msj3 = ""
        if len(contrasena) < 6:
            msj += "\nLa contraseña debe tener por lo menos 6 caracteres."
        for i in contrasena:
            if i.isspace():
                space = True
                msj2 = "La contraseña no debe contener espacios."
            if i.isnumeric():
                num = True
                msj3 = "La contraseña debe tener al menos un número."
        if len(msj) > 0 or num is False or space is True:
            print(msj + "\n" + msj2 + "\n" + msj3)
            return False
        else:
            return True


    def encriptar(self):
        clave = self.contrasena
        cifrado = hashlib.sha256(clave.encode())
        return cifrado.hexdigest()

    
    def actualizarBd(self, lista):
        array = []
        if lista == "usuarios":
            array = self.usuarios
        else:
            array = self.contrasenas
        archivo = open(lista, "w")
        archivo.write(' '.join(array))
        archivo.close()


    def reiniciar(self):
        self.usuario = ""
        self.contrasena = ""


if __name__ == "__main__":
    Registro()